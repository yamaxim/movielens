-- 1. Найти игроков с именем Роналду
SELECT * FROM player
WHERE surname LIKE '%Роналду%'
LIMIT 5;

-- 2. Найти 5 игроков, из команды Польши
SELECT player.surname FROM player
LEFT JOIN team AS t ON player.team_id = t.id
WHERE t.name LIKE 'Польша'
LIMIT 5;

-- 3. Найти стадионы, где играл роналдо
SELECT s.name FROM player
  LEFT JOIN action AS a ON player.id = a.player_id
  LEFT JOIN game AS g ON a.game_id = g.id
  LEFT JOIN stadium AS s ON g.stadium_id = s.id
WHERE player.surname LIKE '%Роналду%'
GROUP BY s.name;

-- 4. Сколько игр сыграл Роналду
SELECT player.name, player.surname, count(g.id) as game_count FROM player
  INNER JOIN action AS a ON player.id = a.player_id
  INNER JOIN game AS g ON a.game_id = g.id
WHERE player.surname LIKE '%Роналду%'
GROUP BY player.name, player.surname
ORDER BY game_count DESC;

-- 5. Найти тренера России
SELECT * FROM coach
INNER JOIN team AS t ON t.coach_id = coach.id
WHERE t.name = 'Россия';

-- 6. Найти 10 самых опытных игроков
SELECT player.name, player.surname, count(g.id) as game_count FROM player
  INNER JOIN action AS a ON player.id = a.player_id
  INNER JOIN game AS g ON a.game_id = g.id
GROUP BY player.name, player.surname
ORDER BY game_count DESC
LIMIT 10;

-- 7. Найти самого неопытного игрока
SELECT player.name, player.surname, count(g.id) as game_count FROM player
  INNER JOIN action AS a ON player.id = a.player_id
  INNER JOIN game AS g ON a.game_id = g.id
GROUP BY player.name, player.surname
ORDER BY game_count ASC
LIMIT 10;

-- 8. Вратари 18 команды по номерам
SELECT * FROM player
WHERE role = 'Вратарь' AND team_id = 18
ORDER BY number;

-- 9. Самый популярный номер среди всех игроков
SELECT number, count(number) as number_count FROM player
GROUP BY number
ORDER BY number_count DESC
LIMIT 1;

-- 10. 10 Игроков, которые забили больше всех пенальти
SELECT player.name, player.surname, COUNT(a.type) as penalty_count FROM player
  INNER JOIN action AS a ON player.id = a.player_id
WHERE a.type = 'penalty goal'
GROUP BY player.name, player.surname, a.type
ORDER BY penalty_count DESC
LIMIT 10;