-- • Необходимо вывести 200 самых популярных фильмов для указанных жанров
-- (Drama, Thriller и Comedy)
SELECT
  ratings.movie_id as id,
  movies.title as movie,
  count(ratings.movie_id) as count
FROM ratings
  LEFT JOIN movies movies ON movies.id = ratings.movie_id
  LEFT JOIN genres_movies genres_movies ON genres_movies.id = movies.id
  LEFT JOIN genres ON genres_movies.genre_id = genres.id
WHERE genres.name IN ('Drama', 'Thriller', 'Comedy')
GROUP BY ratings.movie_id, movies.title
ORDER BY count DESC
LIMIT 10;

-- • Необходимо вывести 50 самых популярных фильмов для указанного списка
-- профессий (Engineer, Programmer и Marketing)
SELECT movies.id,
  movies.title,
  count(r.id) AS rating_count
FROM movies
  LEFT JOIN ratings r ON movies.id = r.movie_id
  LEFT JOIN users u ON r.user_id = u.id
  LEFT JOIN occupations o ON u.occupation_id = o.id
WHERE o.name IN ('Engineer', 'Programmer', 'Marketing')
GROUP BY movies.id
ORDER BY rating_count DESC
LIMIT 50;

-- • Необходимо вывести 200 самых не популярных фильмов, которые были
-- просмотрены пользователями в указанном возрастном диапазоне (от 18 до 35
-- лет)
SELECT movies.id, movies.title, u.age, count(u.age) as age_count FROM movies
  LEFT JOIN ratings r ON movies.id = r.movie_id
  LEFT JOIN users u ON r.user_id = u.id
GROUP BY u.age, movies.id
HAVING u.age > 18 AND u.age < 35
ORDER BY age_count ASC
LIMIT 200;

-- • Необходимо вывести 100 фильмов, снятые в указанный период (c 1993 по
-- 1997 год) с максимальной оценкой пользоватлей (женского пола)
SELECT movies.id, movies.title, movies.release_date, r.rating as female_rating FROM movies
  LEFT JOIN ratings r ON movies.id = r.movie_id
  LEFT JOIN users u ON r.user_id = u.id
WHERE
  movies.release_date > '1993-01-01'
  AND movies.release_date < '1997-01-01'
  AND u.gender = 'F'
ORDER BY female_rating DESC
LIMIT 100;